### Problem 1

Create a HTML page that loads and displays 10 cards from the MTG API.  The cards should be displayed in a table.  The table should tell me the Card's Name, the converted mana cost, and an image of the card.